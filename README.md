My dotfiles and configurations
==============================
These are my dotfiles to configure any of my computers. My configurations probably only make sense for me but can be a good reference for others.

The dotfile management is accomplished with [Dotbot](https://github.com/anishathalye/dotbot) and basically (with modifications) uses the configuration described here (https://github.com/anishathalye/dotbot/wiki/Tips-and-Tricks) for managing multiple machines and profiles. This is a good article explaining dotfile management, [Managing Your Dotfiles](https://www.anishathalye.com/2014/08/03/managing-your-dotfiles/).

## My Setup
I routinely use Mac and Linux for personal and work computers and want to be able to customize my setup on a per system basis. In addition, I want some configurations to be public (this repo) and some to be private (stored on my private BitBucket repo). To handle this I have my private repo as submodule in this repo (`prirvate/`) and use dotbot to setup the appropriate links depending on which profiles I specify with the install script.

## Getting Start Guide

### Clone repo
```
git clone git@github.com:rowoflo/dotfiles

or

git clone https://github.com/rowoflo/dotfiles <path to repos folder>/dotfiles

cd <path to repos folder>/dotfiles
git submodule update --init --recursive
```

### Install profile
```
cd <path to repos folder>/dotfiles
./install <profile>
```

#### Install single configuration
```
cd <path to repos folder>/dotfiles
./install-standalone <configs...>
```

### Example commands
Here are example commands to setup each computer using Dotbot.

* To setup with the default configurations, which are the common configurations across all machines, run
```
./install
```
or
```
./install default
```

* To setup my default ubuntu configurations, which runs the default then the ubuntu specific configurations, run
```
./install ubuntu
```

* To setup my work ubuntu configurations, which runs the default, ubuntu, and then my work
configurations, run
```
./install ubuntu <company>
```

I use the trifecta of Zsh, Vim, Tmux for my development environment, so that is what the majority of these configurations are focused on. In addition, I heavily use Python and Git and have configurations for those as well.

### Zsh
For Zsh I use [oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh) for my Zsh configurations.

*NOTE*: I modified the standard zshrc and split out the parts that come before `source $ZSH/oh-my-zsh.sh` call (into `./.zshrc_pre`) and after (into `./.zshrc_post`). This allows for better customization of each part using my Dotbot profiles.

To include a file in the zshrc add the following
```
if [ -f <path to zshrc to include>]; then
    source <path to zshrc to include>
fi
```

### Vim
For Vim I use [vim-plug](https://github.com/junegunn/vim-plug) as my plugin manager. See my [vimrc](https://github.com/rowoflo/dotfiles/blob/master/vim/vimrc) for a list of plugins that I use.

*NOTE*: `vim-plug` requires `:PlugInstall` to be run from within vim the first time it is opened after setting up a new machine to install the vim plugins.

*NOTE*: `greplace` and `vim-submode` are set to my forked versions of these plugins because they were missing some values in their `.gitignore` files, which was annoying. When the authors of these repos merge my pull request I will change it to the origin plugins. Until then I will periodically pull changes from the original plugins into my forked versions.
```
cd ~/.dotfiles/vim/vim/plugged/greplace
git pull upstream master
cd ~/.dotfiles/vim/vim/plugged/vim-submode
git pull upstream masterr
```

To include a file in the vimrc add the following
```
let $PARENTFILE=expand("<path to vimrc to include>)
if filereadable($PARENTFILE)
    source $PARENTFILE
endif
```

### Tmux
I do not do anything special for my Tmux configurations, but I do change the default prefix to C-SPACE because I like the emacs style key bindings to move the cursor in all my environments (i.e. C-b is used for cursor back one space movement instead of the default prefix).

### Git
To include a file in the gitconfig add the following
```
[include]
    path = <path to gitconfig to include>
```

# Dotbot Setup
The repo is organized to be used with [Dotbot](https://github.com/anishathalye/dotbot), but differs from the Dotbot recommended structure. It is modified to better facilitate re-use of configurations for multiple computers.

## Folder and File Structure

```
dotfiles/
 |
 +- README.md ........................ This file
 |
 +- install .......................... Dotbot install profile script
 |
 +- install_standalone ............... Dotbot install single configuration script
 |
 +- <tool folder>/ ................... Folder containing configuration files (files to be linked) for a given tool
 |  |
 |  +- <configuration file> .......... Configuration file (file to be linked)
 |  |
 |  +- ...
 |  |
 |  +- <configuration file>
 |
 +- ...
 |
 +- <tool folder>/
 |  |
 |  ...
 |
 +- meta/ ........................... Folder containing Dotbot related folders/files
 |  |
 |  +- base.yaml .................... Dotbot base configuration that is applied to all profiles
 |  |
 |  +- default.yaml ................. Dotbot default values
 |  |
 |  +- configs/ ..................... Folder contain Dotbot configuration file (one per tool, named <tool>.yaml)
 |  |  |
 |  |  +- <dotbot tool file>.yaml ... Dotbot configuration file
 |  |  |
 |  |  +- ...
 |  |  |
 |  |  +- <dotbot tool file>.yaml
 |  |
 |  +- profiles/ .................... Folder containing Dotbot profiles
 |  |  |
 |  |  +- <dotbot profile file> ..... Dotbot profile file
 |  |  |
 |  |  +- ...
 |  |  |
 |  |  +- <dotbot profile file>
 |  |
 |  +- dotbot/ ...................... Dotbot submodule repo
 |
 +- private/ ........................ Private configuration repo
```
